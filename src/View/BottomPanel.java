package View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controller.AMSSingleton;
import ams.model.*;

public class BottomPanel extends JPanel {

	private JLabel display;
	
	public BottomPanel()
	{
		display = new JLabel("Waiting for new Program.");
		add(display);
		
	}
	
	public void updateLabel(){
		int coreCourses = 0;
		int electCourses = 0;
		
		//if there are no courses in the AMSModel, print the number of courses as 0 for each and end the method
		if(AMSSingleton.amsSingleton.getAMSModel().getAllCourses() == null){
			String noCourseMessage = "Number of Core Courses: " + coreCourses + " Number of Elective Courses: " + electCourses;
			display.setText(noCourseMessage);
			return;
		}
		
		//for each course in the AMSModel, tally the number of coreCourses and ElectiveCourses
		//then display this number 
		for(Course nextCourse : AMSSingleton.amsSingleton.getAMSModel().getAllCourses()){
			if(nextCourse instanceof CoreCourse){
				coreCourses++;
			}
			else if (nextCourse instanceof ElectiveCourse){
				electCourses++;
			}
		}
		String message = "Number of Core Courses: " + coreCourses + "\t" + "Number of Elective Courses: " + electCourses;
		display.setText(message);
	}
}
