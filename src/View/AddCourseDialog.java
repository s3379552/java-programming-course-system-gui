package View;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddCourseDialog extends JPanel {

	private JPanel addCoursePanel;
	private JTextField cCode;
	private JTextField cName;
	private JComboBox cPointList;
	private JTextField preReqList;
	private JComboBox cTypeList;
	public AddCourseDialog(){
		
		String cTypes[] = {"Core", "Elective"};
		cTypeList = new JComboBox(cTypes);
		
		String cPoints[] = {"6", "12"};
		cPointList = new JComboBox(cPoints);
		
		preReqList = new JTextField(20);
		cCode = new JTextField(20);
		cName = new JTextField(20);

		//add the fields for the course code, name, type, credit points and pre reqs
		addCoursePanel = new JPanel();
		addCoursePanel.add(new JLabel("Course Code: "));
		addCoursePanel.add(cCode);
		addCoursePanel.add(Box.createHorizontalStrut(10)); 
		addCoursePanel.add(new JLabel("Course Name: "));
		addCoursePanel.add(cName);
		addCoursePanel.add(Box.createHorizontalStrut(10));
		addCoursePanel.add(new JLabel("Course Type: "));
		addCoursePanel.add(cTypeList);
		addCoursePanel.add(Box.createHorizontalStrut(10));
		addCoursePanel.add(new JLabel("<html>Credit Points: <br>(Only applies for elective courses)</html>"));
		addCoursePanel.add(cPointList);
		addCoursePanel.add(Box.createHorizontalStrut(10));
		addCoursePanel.add(new JLabel("<html>Prereq codes: <br>(Seperate each course code with a space)</html>"));
		addCoursePanel.add(preReqList);
	}
	
	public JPanel getAddDialog(){
		return addCoursePanel;
	}
	
	public String getCourseCode(){
		return cCode.getText();
	}
	
	public String getCourseName(){
		return cName.getText();
	}
	
	public int getCPoints(){
		String cPointString = (String) cPointList.getSelectedItem();
		return Integer.parseInt(cPointString);
	}
	
	public String getCType(){
		return (String) cTypeList.getSelectedItem();
	}
	
	public String getPreReq(){
		return preReqList.getText();
	}
	
}
