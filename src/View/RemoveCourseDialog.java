package View;

import javax.swing.*;


public class RemoveCourseDialog extends JPanel {

	private JPanel removeCoursePanel;
	private JTextField cCode;
	public RemoveCourseDialog(){
		cCode = new JTextField(20);
		
		removeCoursePanel = new JPanel();
		removeCoursePanel.add(new JLabel("Course Code: "));
		removeCoursePanel.add(cCode);
	}
	
	public JPanel getRemDialog(){
		return removeCoursePanel;
	}
	
	public String getCourseCode(){
		return cCode.getText();
	}
}
