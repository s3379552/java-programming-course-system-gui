package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.*;

import ams.model.Course;

import Controller.AMSSingleton;

public class MiddlePanel extends JPanel {

	public MiddlePanel()
	{	
		GridLayout layout = new GridLayout(0,4);
		this.setLayout(layout);
		layout.preferredLayoutSize(this);
		this.setBackground(Color.WHITE);
	}
	
	public void addCoursePanel(Course course){
		//create a new course panel for the course passed through the method
		//add it to the middle panel
		//create a new scroll pane, tie it to the created course panel
		//add the scroll pane to the middle panel
			NewCoursePanel nextCoursePanel = new NewCoursePanel(course);
			add(nextCoursePanel);
			JScrollPane scrollPane = new JScrollPane(nextCoursePanel);
			this.add(scrollPane);
			repaint();
	}
	
	public void removeCoursePanel(JPanel course){
		this.remove(course);
	}
	
	public void updateCoursePanels(){
		
		//remove all the course panels from the middle panel
		//if no courses exist in the AMSModel's course array, keep the middle panel blank
		//otherwise, for each course in the AMSModel's course array, create a new course panel
		//and add it to the middle panels
		this.removeAll();
		this.updateUI();
		if(AMSSingleton.amsSingleton.getAMSModel().getAllCourses() == null){
			return;
		}
		for(Course nextCourse: AMSSingleton.amsSingleton.getAMSModel().getAllCourses()){
			addCoursePanel(nextCourse);
		}
	}
}
