package View;

import java.awt.event.ActionEvent;

import javax.swing.*;

import ams.model.Program;
import ams.model.facade.AMSModel;

import Controller.AMSSingleton;
import Controller.AddCourseActionListener;
import Controller.NewProgramActionListener;
import Controller.RemoveCourseActionListener;

public class TopPanel extends JPanel {

	private JLabel currentCourse;
	public TopPanel()
	{
		this.currentCourse = new JLabel("N/A");
		JButton np = new JButton("New Program");
		JButton ac = new JButton("Add Course");
		JButton rc = new JButton("Remove Course");
		
		//add the appropriate actionListeners for each of the buttons
		np.addActionListener(new NewProgramActionListener());
		ac.addActionListener(new AddCourseActionListener());
		rc.addActionListener(new RemoveCourseActionListener());
		add(np);
		add(ac);
		add(rc);
		add(currentCourse);
	}
	
	public void updateProgram(Program program)
	{
		currentCourse.setText(program.toString()); 
	}
}
