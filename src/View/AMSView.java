package View;


import java.awt.BorderLayout;

import javax.swing.*;

import ams.model.Course;
import ams.model.facade.AMSModel;

import Controller.AMSSingleton;
import Controller.AMSSingleton;
import Controller.AddCourseActionListener;
import Controller.CloseProgramWindowListener;
import Controller.NewProgramActionListener;
import Controller.RemoveCourseActionListener;
public class AMSView extends JFrame {

	private TopPanel top;
	private MiddlePanel middle;
	private BottomPanel bottom;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem addCourse, remCourse, newProg;
	
	public AMSView()
	{
		super("Drawing Frame");
		
		//create the menu bar
		//create menu items for add course, remove course and new program
		menuBar = new JMenuBar();
		menu = new JMenu("File");
		menuBar.add(menu);
		addCourse = new JMenuItem("Add Course");
		addCourse.addActionListener(new AddCourseActionListener());
		remCourse = new JMenuItem("Remove Course");
		remCourse.addActionListener(new RemoveCourseActionListener());
		newProg = new JMenuItem("New Program");
		newProg.addActionListener(new NewProgramActionListener());
		menu.add(newProg);
		menu.add(addCourse);
		menu.add(remCourse);
		this.setJMenuBar(menuBar);
		
		
		top = new TopPanel();
		middle = new MiddlePanel();
		bottom = new BottomPanel();
		
		
		//add the closeProgram window listener to the AMSView
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new CloseProgramWindowListener());
		
		setLayout(new BorderLayout());
		add(top, BorderLayout.NORTH);
		add(middle, BorderLayout.CENTER);
		add(bottom, BorderLayout.SOUTH);
		
	}
	
	public TopPanel getTop()
	{
		return top;
	}
	
	public MiddlePanel getMiddle()
	{
		return middle;
	}
	
	public BottomPanel getBottom()
	{
		return bottom;
	}
}
