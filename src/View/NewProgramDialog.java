package View;

import javax.swing.*;


public class NewProgramDialog extends JPanel {

	private JPanel newProgramPanel;
	private JTextField pCode;
	private JTextField pName;
	public NewProgramDialog(){
		pCode = new JTextField(20);
		pName = new JTextField(20);
		
		//add fields for the program code and name
		newProgramPanel = new JPanel();
		newProgramPanel.add(new JLabel("Program Code: "));
		newProgramPanel.add(pCode);
		newProgramPanel.add(new JLabel("Program Name: "));
		newProgramPanel.add(pName);
	}
	
	public JPanel getProgDialog(){
		return newProgramPanel;
	}
	
	public String getPCode(){
		return pCode.getText();
	}
	
	public String getPName(){
		return pName.getText();
	}
}
