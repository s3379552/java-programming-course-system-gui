package View;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.*;
import javax.swing.border.Border;

import Controller.AMSSingleton;
import Controller.RemoveCoursePanelMouseListener;
import ams.model.CoreCourse;
import ams.model.Course;

public class NewCoursePanel extends JPanel {

	private JPanel panel;
	private JLabel desc;
	private JScrollPane scrollText;
	
	//create a course panel with a flow layout, description label, and light gray background,
	//and a mouse listener for the panel
	public NewCoursePanel(Course course){
		FlowLayout layout = new FlowLayout();
		this.setLayout(layout);
		desc = new JLabel();
		this.add(desc);
		this.setBackground(Color.LIGHT_GRAY);
		setText(course);
		this.addMouseListener(new RemoveCoursePanelMouseListener(this, course));
	}
	
	public void setText(Course course){
		//create seperate strings for the preReqs, code, title and credit points
		//get the user's input, add it to each of the strings
		String preReq = "";
		String code = course.getCode();
		String title = course.getTitle();
		int cPoints = course.getCreditPoints();
		if(course.getPreReqs() == null){
			preReq = "None";
		}
		else{
			for(String nextPreReq: course.getPreReqs()){
				preReq = preReq + nextPreReq + ", ";
			}
		}
		//add all the strinngs to the description label
		desc.setText("<html>Code: " + code + "<br>Title: " + title + 
				"<br>Credit Points: " + cPoints + 
				"<br>Prereqs: " + preReq + "</html>");
		
		//if the linked course is a CoreCourse
		//add a 10 pixel border to the panel
		if(course instanceof CoreCourse){
			Border blackLine = BorderFactory.createLineBorder(Color.black, 10);
			this.setBorder(blackLine);
		}
	}
}
