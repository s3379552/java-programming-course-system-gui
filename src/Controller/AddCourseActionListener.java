package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import ams.model.CoreCourse;
import ams.model.ElectiveCourse;
import ams.model.exception.ProgramException;

import View.AddCourseDialog;


public class AddCourseActionListener implements ActionListener {

	private AddCourseDialog courseDialog;
	public AddCourseActionListener() {}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		courseDialog = new AddCourseDialog();
		
		//display a dialog box containing all the fields created from the AddCourseDialog class
		int result = JOptionPane.showConfirmDialog(null, courseDialog.getAddDialog(), "Add Course", JOptionPane.OK_CANCEL_OPTION);
		
		//if the code and name fields are empty, return an error dialog
		if(courseDialog.getCourseCode().isEmpty() || courseDialog.getCourseName().isEmpty()){
			JOptionPane.showMessageDialog(null, "Please enter a course code and name.", "Add Course Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if(result == JOptionPane.OK_OPTION){
			
			//if the course selected is a core course
			//create a new CoreCourse object with the given values
			//try to add it to the AMSModel's course array
			//if an exception is thrown, display the exception message in a new dialog
			if(courseDialog.getCType() == "Core"){
				CoreCourse cCore;
				if(courseDialog.getPreReq().isEmpty()){
					cCore = new CoreCourse(courseDialog.getCourseCode(), courseDialog.getCourseName(), null);				
				}
				else {
					String[] preReqs = courseDialog.getPreReq().split(" ");
					cCore = new CoreCourse(courseDialog.getCourseCode(), courseDialog.getCourseName(), preReqs);
				}
				try {
					AMSSingleton.amsSingleton.getAMSModel().addCourse(cCore);
					AMSSingleton.amsSingleton.getAMSView().getMiddle().addCoursePanel(cCore);
				} catch (ProgramException e){
					JOptionPane.showMessageDialog(null, e.getMessage(), "Add Course Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			
			//if the course selected is an elective course
			//create a new ElectiveCourse object with the given values (including the selected Credit Point value)
			//try to add it to the AMSModel's course array
			//if and exception is thrown, display the exception message in a new dialog
			if(courseDialog.getCType() == "Elective"){
				ElectiveCourse cElect;
				if(courseDialog.getPreReq().isEmpty()){
					cElect = new ElectiveCourse(courseDialog.getCourseCode(), courseDialog.getCourseName(), null);
				}
				else {
					String[] preReqs = courseDialog.getPreReq().split(" ");
					cElect = new ElectiveCourse(courseDialog.getCourseCode(), courseDialog.getCourseName(), 
						courseDialog.getCPoints(), preReqs);
				}
				try {
					AMSSingleton.amsSingleton.getAMSModel().addCourse(cElect);
					AMSSingleton.amsSingleton.getAMSView().getMiddle().addCoursePanel(cElect);
				} catch (ProgramException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Add Course Error", JOptionPane.ERROR_MESSAGE);
				}
			}
				AMSSingleton.amsSingleton.getAMSView().getBottom().updateLabel();	
		}

	}

}
