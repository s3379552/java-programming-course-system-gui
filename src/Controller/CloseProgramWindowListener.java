package Controller;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class CloseProgramWindowListener implements WindowListener {
	
	public CloseProgramWindowListener(){
		
	}

	//windowClosing method (and included exit method) taken from
	//http://iitdu.forumsmotion.com/t593-java-swing-adding-confirmation-dialogue-for-closing-window-in-jframe
	@Override
	public void windowClosing(WindowEvent arg0) {
		  int result = JOptionPane.showConfirmDialog(null,
                  "Are you sure you want to exit?", "Exit",
                  JOptionPane.YES_NO_OPTION);
		  if (result == JOptionPane.YES_OPTION)
			  exit(AMSSingleton.amsSingleton.getAMSView());

	}
	
	public static void exit(JFrame j){
		j.dispose();
	}
	
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

}
