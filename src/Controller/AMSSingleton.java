package Controller;
import ams.model.facade.AMSFacade;
import ams.model.facade.AMSModel;

import View.AMSView;

public class AMSSingleton {

	public static final AMSSingleton amsSingleton = new AMSSingleton();

	static
	{
		amsSingleton.model = new AMSFacade();
		amsSingleton.view = new AMSView();
	}
	
	private AMSModel model;
	private AMSView view;
	private AMSSingleton()
	{
		this.model = null;
		this.view = null;
	}
	
	public AMSModel getAMSModel()
	{
		return model;
	}
	
	public AMSView getAMSView()
	{
		return view;
	}
	
	public static void main(String[] args)
	{
		AMSSingleton.amsSingleton.getAMSView().setVisible(true);
	}
}
