package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ams.model.Course;
import ams.model.exception.ProgramException;

public class RemoveCoursePanelMouseListener implements MouseListener {

	private JPanel CoursePanel;
	private Course linkedCourse;
	public RemoveCoursePanelMouseListener(JPanel panel, Course course){
		CoursePanel = panel;
		linkedCourse = course;
	}

	//Show a dialog box asking the user to confirm the removal of the course
	//if they select yes, remove the course from the AMSModel
	//update the bottom label
	//update the middle panel
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this course from the program?", 
	               "Remove Course Confirmation", JOptionPane.OK_CANCEL_OPTION);
		if(confirm == JOptionPane.OK_OPTION){
			try {
				AMSSingleton.amsSingleton.getAMSModel().removeCourse(linkedCourse.getCode());
			} catch (ProgramException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Remove Course Error", JOptionPane.ERROR_MESSAGE);
			}
			AMSSingleton.amsSingleton.getAMSView().getBottom().updateLabel();
			AMSSingleton.amsSingleton.getAMSView().getMiddle().updateCoursePanels();
			AMSSingleton.amsSingleton.getAMSView().repaint();
			return;
		}
		return;		

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
