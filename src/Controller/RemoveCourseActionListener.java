package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ams.model.Course;
import ams.model.exception.ProgramException;

import View.RemoveCourseDialog;

public class RemoveCourseActionListener implements ActionListener {

	private RemoveCourseDialog remCourse;
	private JPanel coursePanel;
	public RemoveCourseActionListener(){

	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		remCourse = new RemoveCourseDialog();
		
		//show a dialog with the input form from the created RemoveCourseDialog object
		//if the course code is empty, show an error message dialog
		int result = JOptionPane.showConfirmDialog(null, remCourse.getRemDialog(), "Remove Course", JOptionPane.OK_CANCEL_OPTION);
		if(remCourse.getCourseCode().isEmpty()){
			JOptionPane.showMessageDialog(null, "Please enter a course code.", "Remove Course Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//loop through the courses in the AMSModel
		//if the currently selected course's code matches the input code
		//show a dialog asking the user to confirm the coures's removal
		//if they accept, try to remove the course
		//if an exception is thrown, show an error dialog
		//otherwise, remove the course and update the bottom and middle panels
		if(result == JOptionPane.OK_OPTION){
			for(Course nextCourse : AMSSingleton.amsSingleton.getAMSModel().getAllCourses()){
				if(nextCourse.getCode().equalsIgnoreCase(remCourse.getCourseCode())){
					int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this course from the program?", 
				               "Remove Course Confirmation", JOptionPane.OK_CANCEL_OPTION);
					if(confirm == JOptionPane.OK_OPTION){
						try {
							AMSSingleton.amsSingleton.getAMSModel().removeCourse(remCourse.getCourseCode());
						} catch (ProgramException e) {
							JOptionPane.showMessageDialog(null, e.getMessage(), "Remove Course Error", JOptionPane.ERROR_MESSAGE);
						}
						AMSSingleton.amsSingleton.getAMSView().getBottom().updateLabel();
						AMSSingleton.amsSingleton.getAMSView().getMiddle().updateCoursePanels();
						AMSSingleton.amsSingleton.getAMSView().repaint();
						return;
					}
					return;
				}
			}
			//if the course is not found, show a message dialog telling the user the course was not found
			JOptionPane.showMessageDialog(null, "Course code not found!", "Remove Course Error", JOptionPane.ERROR_MESSAGE);
		}
	}

}
