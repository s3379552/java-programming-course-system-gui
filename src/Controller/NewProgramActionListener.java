package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import View.AMSView;
import View.AddCourseDialog;
import View.NewProgramDialog;
import ams.model.Program;

public class NewProgramActionListener implements ActionListener {

	private NewProgramDialog progDialog;
	public NewProgramActionListener() {}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		progDialog = new NewProgramDialog();
		
		//show a dialog with the course code and name fields from the NewProgramDialog object
		//if the code field or name field is empty, display an error dialog
		//otherwise, add the program to the AMSModel, update the bottom panel and refresh the middle panel
		int result = JOptionPane.showConfirmDialog(null, progDialog.getProgDialog(), "Add Course", JOptionPane.OK_CANCEL_OPTION);
		if(progDialog.getPCode().isEmpty() || progDialog.getPName().isEmpty()){
			JOptionPane.showMessageDialog(null, "Please enter a program code and name.", "New Program Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if(result == JOptionPane.OK_OPTION){
			Program p = new Program(progDialog.getPCode(), progDialog.getPName());
			AMSSingleton.amsSingleton.getAMSModel().addProgram(p);
			AMSSingleton.amsSingleton.getAMSView().getTop().updateProgram(p);
			AMSSingleton.amsSingleton.getAMSView().getBottom().updateLabel();
			AMSSingleton.amsSingleton.getAMSView().getMiddle().updateCoursePanels();
		}	
	}

}
