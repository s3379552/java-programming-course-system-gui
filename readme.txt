Student number - s3379552

Errors:
	The course panels do not resize to take up all the available space when there are less than 4 course panels.

Possible workarounds:
	I did not figure out a workaround after trying multiple layout ideas, but I suspect it has something to do with addinng an extra layout to the middle panel.

Operation:
	File menu: Contains extra buttons for new programs, new courses and removing courses.
	Course Panels: Clicking on a course panel will bring up the confirmation prompt to remove the course.
	Exit Program: Clicking the close button in the top right will bring up the exit dialog.
	New Course Dialog: If the 'core' course type is selected, the credit points combo box will not change the course points, even though it is still changeable/selectable.
			This combo box will still change the credit points value if the 'elective' course type is selected.